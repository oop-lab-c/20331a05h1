#include<iostream>
using namespace std;
class person
{
    public:
    person()
    {
        cout<<" person is sleeping"<<endl;
    }
};
class person1:public person
{
    public:
    person1()
    {
        cout<<" person1 is sleeping"<<endl;
    }
};
class person2:public person
{
    public:
    person2()
    {
        cout<<" person2 is sleeping"<<endl;
    }
};
class person3:public person1,public person2
{
    public:
    person3()
    {
        cout<<" person3 is sleeping"<<endl;
    }
};
int main()
{
    person3 obj;
    return 0;
}
