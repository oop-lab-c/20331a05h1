import java.util.*;
interface Abs {
    void show();

}

class PureAbsJava implements Abs {
    public void show() {
        System.out.println("welcome to mvgr college of engineering");
    }

    public static void main(String[] args) {
        PureAbsJava b = new PureAbsJava();
        b.show();

    }

}


