import java.util.*;
class MyException extends Exception{
     MyException(int n){
        System.out.println("age must be above 18");
    }
}
class Demo{
    public static void main(String[] args){
        System.out.println("enter your age");
        Scanner i = new Scanner(System.in);
        int age=i.nextInt();
        if(age>18){
            System.out.println("eligible to get driving license");
        }
        else{
            try{
                throw new MyException(age);
            }catch(Exception e)
            {
                System.out.println(e);
            }
            finally{
                System.out.println("Exception handled");
            }
        }
    }
} 
